const EQUIPMENT_TYPE = {
  WPN: 'WPN',
  AMR: 'AMR',
};

const CLASS_TYPE = {
  SABER: 1,
  ARCHER: 2,
  LANCER: 3,
  BERSERKER: 4,
  CASTER: 5,
  RIDER: 6,
  FOREIGNER: 7,
};

const GAME_STATE = {
  MENU: 'menu',
  LOGIN: 'login',
  ACCOUNT: 'account',
  CHARACTERSELECT: 'character-select',
  CHARACTERDISPLAY: 'character-display',
  INGAME: 'ingame',
  RESULT: 'result',
  DUNGEONSELECT: 'dungeon-select',
};
