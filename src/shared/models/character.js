export class Character {
  contructor(id, name, classType) {
    this._id = id;
    this.classType = classType;
    this.name = name;
  }

  _id = '';
  classType = 0;
  name = '';
  accountId = '';
  nextLevelExp = 0;
  totalExp = 0;
  level = 0;
  stats = {};
  equipment = {};
  dungeonAccess = [];
  skills = [];
}
