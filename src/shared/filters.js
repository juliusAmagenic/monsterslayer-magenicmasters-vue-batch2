import Vue from 'vue';

Vue.filter('truncate', (value, length = 10) => {
  if (!value) {
    return '';
  }

  let truncated = value.substring(0, length);

  if (value.length > length) {
    truncated = truncated + '...';
  }

  return truncated;
});
