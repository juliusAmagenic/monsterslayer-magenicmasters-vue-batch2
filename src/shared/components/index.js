import Vue from 'vue';
import Loader from './Loader.vue';

Vue.component('app-loader', Loader);
