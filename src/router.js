import LoginComponent from './components/states/LoginComponent';
import MenuComponent from './components/states/MenuComponent';
import AccountCreationComponent from './components/states/AccountCreationComponent';
import CharacterSelectComponent from './components/states/CharacterSelectComponent';
import CharacterDisplayComponent from './components/states/CharacterDisplayComponent';
import DungeonSelectComponent from './components/states/DungeonSelectComponent';
import InGameComponent from './components/states/InGameComponent';
import BattleResultComponent from './components/states/BattleResultComponent';

const beforeEnter = function(to, from, next) {
  const accountId = sessionStorage.getItem('accountId');
  if (!accountId) {
    next('/login');
  } else {
    next();
  }
};

export const routes = [
  {
    path: '/login',
    component: LoginComponent,
  },
  {
    path: '/character-select',
    component: CharacterSelectComponent,
  },
  {
    path: '/character-display',
    component: CharacterDisplayComponent,
  },
  {
    path: '/dungeon-select',
    component: DungeonSelectComponent,
  },
  {
    path: '/account',
    component: AccountCreationComponent,
  },
  {
    path: '/ingame',
    component: InGameComponent,
    beforeEnter,
  },
  {
    path: '/result',
    component: BattleResultComponent,
  },
  {
    path: '*',
    component: MenuComponent,
  },
];
