export const Spriggan = {
  name: 'Spriggan',
  className: 'Saber',
  detail: {
    image: 'spriggan.png',
    posNormal: {
      x: '-1000px',
      y: '-500px',
    },
  },
  sprite: {
    image: 'spriggan.png',
    size: 400,
  },

  isTurn: true,

  skills: [
    {
      id: 0,
      genericname: 'Damage',
      name: 'Stone Smash',
      desc: 'Strikes the enemy with the blunted stone sword',
      mpCost: 15,
      cooldown: 1,
      turnCooldown: 0,
      message: '<actor> strikes <receiver> with its blunted sword! ',
      effects: [
        {
          type: 'hpdamage',
          target: 'enemy',
          damage: 0.5,
        },
      ],
    },
    {
      id: 1,
      genericname: 'Atk +',
      name: 'Charge Attack',
      desc: 'Enhances next attack greatly. Stuns user for 2 turns.',
      cooldown: 4,
      turnCooldown: 0,
      message: '<actor> charges!',
      mpCost: 40,
      effects: [
        {
          type: 'status',
          target: 'self',
          status: 'atkup', //put this on enum
          modifier: 2,
          message: 'Increase <actor> attack greatly for <turn> turns',
          turns: 3,
        },
        {
          target: 'self',
          type: 'status',
          status: 'stun', //put this on enum
          message: 'Stun <actor> for <turn> turns',
          turns: 2,
        },
      ],
    },
    {
      id: 2,
      genericname: 'Def +',
      name: 'Harden',
      desc: 'Increases defense greatly for 6 turns. Stuns the user for 2 turns.',
      cooldown: 9,
      turnCooldown: 0,
      message: '<actor> is building its defense! ',
      mpCost: 40,
      effects: [
        {
          target: 'self',
          type: 'status',
          status: 'stun', //put this on enum
          message: 'Stun <actor> for <turn> turns',
          turns: 2,
        },
        {
          type: 'status',
          target: 'self',
          status: 'defup', //put this on enum
          modifier: 0.75,
          message: 'Increases <actor> defense for <turn> turns',
          turns: 6,
        },
      ],
    },
  ],
  sounds: null,

  baseAttack: 10,
  currentAttack: 10,

  hpRegenRate: 15,
  maxHp: 250,
  currentHp: 250,
  mpRegenRate: 5,
  maxMp: 120,
  currentMp: 120,

  ailments: [],
};

export const DemonHunter = {
  name: 'Demon Hunter',
  className: 'Caster',
  detail: {
    image: 'demonhunter.png',
    posNormal: {
      x: '-500px',
      y: '-100px',
    },
  },
  sprite: {
    image: 'demonhunter.png',
    size: 300,
  },

  isTurn: true,

  skills: [
    {
      id: 0,
      genericname: 'Damage',
      name: 'Heavy Claw',
      desc: 'Strikes the enemy with massive claws',
      mpCost: 15,
      cooldown: 1,
      turnCooldown: 0,
      message: '<actor> strikes <receiver> with its massive claws! ',
      effects: [
        {
          type: 'hpdamage',
          target: 'enemy',
          damage: 0.25,
        },
      ],
    },
    {
      id: 1,
      genericname: 'Mix -',
      name: 'Demonic Aura',
      desc: 'Decrease enemy defense for 4 turns. Decrease enemy attack for 2 turns. Cooldown of 6 turns',
      cooldown: 6,
      turnCooldown: 0,
      message: '<actor> intimidates <receiver> with its presence! ',
      mpCost: 30,
      effects: [
        {
          type: 'status',
          target: 'enemy',
          status: 'defdown', //put this on enum
          modifier: 0.5,
          message: 'Decrease <receiver> defense for <turn> turns',
          turns: 4,
        },
        {
          type: 'status',
          target: 'enemy',
          status: 'atkdown', //put this on enum
          modifier: 0.5,
          message: 'Decrease <receiver> attack for <turn> turns',
          turns: 2,
        },
      ],
    },
    {
      id: 2,
      genericname: 'HP +',
      name: 'Ritual of the Damn',
      desc: 'Regenerates Health but stuns the user. Regenerates HP for 3 turns and Stuns user for 2 turns.',
      cooldown: 6,
      turnCooldown: 0,
      message: '<actor> started the Ritual of the Damn. ',
      mpCost: 60,
      effects: [
        {
          target: 'self',
          type: 'status',
          status: 'stun', //put this on enum
          message: 'Stun <actor> for <turn> turns',
          turns: 2,
        },
        {
          type: 'status',
          target: 'self',
          status: 'hpregen', //put this on enum
          modifier: 5,
          message: 'Regenerates <actor> HP for <turn> turns',
          turns: 3,
        },
      ],
    },
  ],
  sounds: null,

  baseAttack: 10,
  currentAttack: 10,

  hpRegenRate: 7,
  maxHp: 170,
  currentHp: 170,
  mpRegenRate: 5,
  maxMp: 120,
  currentMp: 120,

  ailments: [],
};
