export const Characters = [
  {
    // Saber
    isEnabled: true,
    id: 1,
    className: 'Saber',
    description: 'A swordsman who focuses on physical strength, speed, and agility.',
    sprite: {
      image: 'saber',
      size: 170,
    },
    detail: {
      image: 'saber_1',
      posNormal: {
        x: '-5px',
        y: '-710px',
      },
    },
    sounds: {},
    maxHp: 140,
    maxMp: 120,
    baseAttack: 15,
    hpRegenRate: 5,
    mpRegenRate: 7,
    skills: [
      {
        id: 0,
        genericname: 'Stun',
        name: 'Quick Slash',
        desc: 'Deals slight damage and stuns the opponent for 1 turn',
        mpCost: 15,
        cooldown: 2,
        turnCooldown: 0,
        message: '<actor> strikes <receiver> with haste!',
        effects: [
          {
            type: 'hpdamage',
            target: 'enemy',
            damage: 0.1,
          },
          {
            target: 'enemy',
            type: 'status',
            status: 'stun', //put this on enum
            message: 'Stun <receiver> for <turn> turns',
            turns: 1,
          },
        ],
      },
      {
        id: 1,
        genericname: 'Def +',
        name: 'Riposte',
        desc: 'Increase defense for 1 turn and increase attack for 2 turns',
        mpCost: 25,
        cooldown: 3,
        turnCooldown: 0,
        message: '<actor> entered its stance',
        effects: [
          {
            target: 'self',
            type: 'status',
            status: 'atkup', //put this on enum
            modifier: 0.25,
            message: 'Increase attack of <actor> for <turn> turns',
            turns: 2,
          },
          {
            target: 'self',
            type: 'status',
            status: 'defup', //put this on enum
            modifier: 0.25,
            message: 'Increase defense of <actor> for <turn> turns',
            turns: 1,
          },
        ],
      },
      {
        id: 2,
        genericname: 'Damage',
        name: 'The Sword of Promised Victory',
        desc: 'Deals heavy damage',
        mpCost: 75,
        cooldown: 9,
        turnCooldown: 0,
        message: '<actor>: "EKUSU----KALIBAAAAAAAAAAAAAAAAAAAAAAAAAAAAA!!!!!!"',
        effects: [
          {
            type: 'hpdamage',
            target: 'enemy',
            damage: 4,
          },
        ],
      },
    ],
  },
  {
    // Archer
    isEnabled: true,
    id: 2,
    className: 'Archer',
    description: 'Warrior who fights using long-range weapons. Focuses agility and dexterity.',
    sprite: {
      image: 'archer',
      size: 200,
    },
    detail: {
      image: 'archer_1',
      posNormal: {
        x: '-5px',
        y: '-773px',
      },
    },
    sounds: {},
    maxHp: 100,
    maxMp: 150,
    baseAttack: 17,
    hpRegenRate: 4,
    mpRegenRate: 6,
    skills: [
      {
        id: 0,
        genericname: 'Atk +',
        name: 'Charge',
        desc: 'Increase next attack heavily',
        mpCost: 35,
        cooldown: 4,
        turnCooldown: 0,
        message: '<actor> started charging its bow',
        effects: [
          {
            target: 'self',
            type: 'status',
            status: 'atkup', //put this on enum
            modifier: 0.75,
            message: 'Greatly increase attack of <actor> for <turn> turns',
            turns: 1,
          },
        ],
      },
      {
        id: 1,
        genericname: 'Stun',
        name: 'Fake Out',
        desc: 'Stuns enemy for 3 turns',
        mpCost: 45,
        cooldown: 7,
        turnCooldown: 0,
        message: '<actor> surprised <receiver> with a sneak attack',
        effects: [
          {
            target: 'enemy',
            type: 'status',
            status: 'stun', //put this on enum
            message: 'Stun <receiver> for <turn> turns',
            turns: 3,
          },
        ],
      },
      {
        id: 2,
        genericname: 'Damage',
        name: 'Stella',
        desc: 'Deals damage and stuns the enemy for 2 turns',
        mpCost: 55,
        cooldown: 7,
        turnCooldown: 0,
        message: '<actor> strikes <receiver> vital points!',
        effects: [
          {
            type: 'hpdamage',
            target: 'enemy',
            damage: 1,
          },
          {
            target: 'enemy',
            type: 'status',
            status: 'stun', //put this on enum
            message: 'Stun <receiver> for <turn> turns',
            turns: 2,
          },
        ],
      },
    ],
  },
  {
    // Lancer
    isEnabled: true,
    id: 3,
    className: 'Lancer',
    description: 'Knight that fights with spears. Defensive and sturdy.',
    sprite: {
      image: 'lancer',
      size: 270,
    },
    detail: {
      image: 'lancer_1',
      posNormal: {
        x: '-5px',
        y: '-773px',
      },
    },
    sounds: {},
    maxHp: 170,
    maxMp: 120,
    baseAttack: 13,
    hpRegenRate: 6,
    mpRegenRate: 4,
    skills: [
      {
        id: 0,
        genericname: 'Def +',
        name: 'Dark Canopy',
        desc: 'Increase defense by 3 turns',
        mpCost: 35,
        cooldown: 4,
        turnCooldown: 0,
        message: '<actor> started spinning its weapon',
        effects: [
          {
            target: 'self',
            type: 'status',
            status: 'defup', //put this on enum
            modifier: 0.25,
            message: 'Increase defense of <actor> for <turn> turns',
            turns: 3,
          },
        ],
      },
      {
        id: 1,
        genericname: 'Atk +',
        name: 'Battle Cry',
        desc: 'Increase Attack and Defense for 2 turns. Decrease Attack and Defense of enemy for 2 turns',
        mpCost: 45,
        cooldown: 6,
        turnCooldown: 0,
        message: '<actor>: "TONIGHT, WE DINE IN HELL!"',
        effects: [
          {
            target: 'enemy',
            type: 'status',
            status: 'atkdown', //put this on enum
            modifier: 0.25,
            message: 'Decrease defense of <receiver> for <turn> turns',
            turns: 2,
          },
          {
            target: 'enemy',
            type: 'status',
            status: 'defdown', //put this on enum
            modifier: 0.25,
            message: 'Decrease defense of <receiver> for <turn> turns',
            turns: 2,
          },
          {
            target: 'self',
            type: 'status',
            status: 'atkup', //put this on enum
            modifier: 0.25,
            message: 'Increase attack of <actor> for <turn> turns',
            turns: 2,
          },
          {
            target: 'self',
            type: 'status',
            status: 'defup', //put this on enum
            modifier: 0.25,
            message: 'Increase defense of <actor> for <turn> turns',
            turns: 2,
          },
        ],
      },
      {
        id: 2,
        genericname: 'Def -',
        name: 'Cursed Series: Spear of Rage',
        desc: 'Deals damage to the enemy and greatly weaken their defense for 5 turns',
        mpCost: 60,
        cooldown: 8,
        turnCooldown: 0,
        message: '<actor> surrendered to its rage!',
        effects: [
          {
            type: 'hpdamage',
            target: 'enemy',
            damage: 0.1,
          },
          {
            target: 'enemy',
            type: 'status',
            status: 'defdown', //put this on enum
            modifier: 0.75,
            message: 'Greately decrease defense of <receiver> for <turn> turns',
            turns: 5,
          },
        ],
      },
    ],
  },
  {
    // Berserker
    isEnabled: true,
    id: 4,
    className: 'Berserker',
    description: 'Warrior with increased Attack at the cost of Defense.',
    sprite: {
      image: 'berserker',
      size: 300,
    },
    detail: {
      image: 'berserker_1',
      posNormal: {
        x: '-5px',
        y: '-720px',
      },
    },
    sounds: {},
    maxHp: 180,
    maxMp: 100,
    baseAttack: 20,
    hpRegenRate: 6,
    mpRegenRate: 3,
    skills: [
      {
        id: 0,
        genericname: 'Damage',
        name: 'Thrash',
        desc: 'Deals increased damage',
        mpCost: 10,
        cooldown: 1,
        turnCooldown: 0,
        message: '<actor> is thrashing <receiver>!',
        effects: [
          {
            type: 'hpdamage',
            target: 'enemy',
            damage: 0.75,
          },
        ],
      },
      {
        id: 1,
        genericname: 'Atk +',
        name: 'Insanity Break',
        desc: 'Greatly increases Attack for 2 turns but greatly decreases Defense for 4 turns',
        mpCost: 25,
        cooldown: 5,
        turnCooldown: 0,
        message: '<actor> sets its sight on enemy!',
        effects: [
          {
            target: 'self',
            type: 'status',
            status: 'atkup', //put this on enum
            modifier: 0.5,
            message: 'Greatly increase attack of <actor> for <turn> turns',
            turns: 2,
          },
          {
            target: 'self',
            type: 'status',
            status: 'defdown', //put this on enum
            modifier: 0.75,
            message: 'Greately decrease defense of <actor> for <turn> turns',
            turns: 4,
          },
        ],
      },
      {
        id: 2,
        genericname: 'Damage',
        name: 'Final Strike',
        desc: 'Deals heavy damage and stuns the user for 5 turns.',
        mpCost: 100,
        cooldown: 9,
        turnCooldown: 0,
        message: '<actor> is pouring everything for this strike!',
        effects: [
          {
            type: 'hpdamage',
            target: 'enemy',
            damage: 3.5,
          },
          {
            target: 'self',
            type: 'status',
            status: 'stun', //put this on enum
            message: 'Stun <actor> for <turn> turns',
            turns: 5,
          },
        ],
      },
    ],
  },
  {
    // Caster
    isEnabled: true,
    id: 5,
    className: 'Caster',
    description: "Magician that focuses on decreasing the enemy's ability to fight and increase its own.",
    sprite: {
      image: 'caster',
      size: 160,
    },
    detail: {
      image: 'caster_1',
      posNormal: {
        x: '-5px',
        y: '-773px',
      },
    },
    sounds: {},
    maxHp: 90,
    maxMp: 190,
    baseAttack: 11,
    hpRegenRate: 3,
    mpRegenRate: 9,
    skills: [
      {
        id: 0,
        genericname: 'Def -',
        name: 'Soul Chill',
        desc: 'Decreases defense of the enemy for 2 turns and stuns enemy for 2 turns',
        mpCost: 20,
        cooldown: 4,
        turnCooldown: 0,
        message: '<actor> casted shivering spell to <receiver>',
        effects: [
          {
            target: 'enemy',
            type: 'status',
            status: 'defdown', //put this on enum
            modifier: 0.25,
            message: 'Decrease defense of <receiver> for <turn> turns',
            turns: 2,
          },
          {
            target: 'enemy',
            type: 'status',
            status: 'stun', //put this on enum
            message: 'Stun <receiver> for <turn> turns',
            turns: 2,
          },
        ],
      },
      {
        id: 1,
        genericname: 'Atk +',
        name: 'Soul Siphon',
        desc: 'Increase attack for 3 turns and decreases attack of enemy for 3 turns and steals enemy MP',
        mpCost: 25,
        cooldown: 4,
        turnCooldown: 0,
        message: '<actor> sucked <receiver> of its attack power',
        effects: [
          {
            target: 'enemy',
            type: 'status',
            status: 'atkdown', //put this on enum
            modifier: 0.25,
            message: 'Decrease attack of <receiver> for <turn> turns',
            turns: 3,
          },
          {
            target: 'self',
            type: 'status',
            status: 'atkup', //put this on enum
            modifier: 0.25,
            message: 'Increase attack of <actor> for <turn> turns',
            turns: 3,
          },
          {
            target: 'enemy',
            type: 'mpdamage',
            damage: 2,
            message: 'Steals <receiver> MP',
          },
          {
            target: 'self',
            type: 'status',
            status: 'mpregen',
            modifier: 5,
            message: 'Converted <receiver> MP to <actor> MP',
            turns: 1,
          },
        ],
      },
      {
        id: 2,
        genericname: 'Atk +',
        name: 'Rite of Ascension',
        desc: 'Regenerates Hp for 3 turns and Greatly Increases attack for 3 turns',
        mpCost: 60,
        cooldown: 5,
        turnCooldown: 0,
        message: '<actor> enhances its own ability',
        effects: [
          {
            target: 'self',
            type: 'status',
            status: 'atkup', //put this on enum
            modifier: 0.75,
            message: 'Greatly increases attack of <actor> for <turn> turns',
            turns: 3,
          },
          {
            target: 'self',
            type: 'status',
            status: 'hpregen', //put this on enum
            modifier: 10,
            message: '<actor> regenerates health for <turn> turns',
            turns: 3,
          },
        ],
      },
    ],
  },
  {
    // Rider
    isEnabled: false,
    id: 6,
    className: 'Rider',
    description: 'Warriors kings and queens that lead the people and inspire them.',
    sprite: {
      image: 'rider',
      size: 200,
    },
    detail: {
      image: 'rider_1',
      posNormal: {
        x: '-5px',
        y: '-653px',
      },
    },
    sounds: {},
    maxHp: 90,
    maxMp: 190,
    baseAttack: 11,
    hpRegenRate: 3,
    mpRegenRate: 9,
    skills: [
      {
        id: 0,
        genericname: 'Atk +',
        name: 'Charisma',
        desc: 'Increase Attack for 3 turns',
        mpCost: 10,
        cooldown: 4,
        turnCooldown: 0,
        message: '<actor> is glowing with greatness!',
        effects: [
          {
            target: 'self',
            type: 'status',
            status: 'atkup', //put this on enum
            message: 'Increase attack for <turn> turns',
            modifier: 0.25,
            turns: 3,
          },
        ],
      },
      {
        id: 1,
        genericname: 'Damage',
        name: 'Stampede',
        desc: 'Attack the enemy and stun them for 2 turns',
        mpCost: 25,
        cooldown: 5,
        turnCooldown: 0,
        message: '<actor> started running towards <receiver>!',
        effects: [
          {
            target: 'enemy',
            type: 'status',
            status: 'stun', //put this on enum
            message: '<receiver> was stunned for <turn> turns',
            turns: 2,
          },
          {
            target: 'enemy',
            type: 'hpdamage',
            damage: 0.25,
            message: '<actor> ran over <receiver>!',
          },
        ],
      },
      {
        id: 2,
        genericname: 'Atk +',
        name: 'Final Showdown',
        desc: 'Greatly increase attack of both user and enemy for 2 turns. Stun enemy for 1 turn.',
        mpCost: 60,
        cooldown: 5,
        turnCooldown: 0,
        message: '<actor> provoked <receiver> to a showdown!',
        effects: [
          {
            target: 'self',
            type: 'status',
            status: 'atkup', //put this on enum
            modifier: 0.75,
            message: 'Greatly increases attack of <actor> for <turn> turns',
            turns: 2,
          },
          {
            target: 'enemy',
            type: 'status',
            status: 'atkup', //put this on enum
            modifier: 0.75,
            message: 'Greatly increases attack of <receiver> for <turn> turns',
            turns: 2,
          },
          {
            target: 'enemy',
            type: 'status',
            status: 'stun', //put this on enum
            message: '<receiver> was stunned for <turn> turns',
            turns: 1,
          },
        ],
      },
    ],
  },
  {
    // Foreigner
    isEnabled: false,
    id: 7,
    className: 'Foreigner',
    description: 'A foreign entity which are summoned outside of the world that we know.',
    name: 'Abigail Williams',
    detail: {
      image: 'Abigail 2',
      posNormal: {
        x: '-5px',
        y: '-773px',
      },
    },

    isTurn: true,

    sprite: {
      image: 'abigailwilliams1',
      size: 200,
    },

    sounds: {
      attack: ['AbigailWilliamsAttack1.ogg', 'AbigailWilliamsOuterAttack2.ogg'],
      skills: [
        'AbigailWilliamsSkill1.ogg',
        'AbigailWilliamsSkill2.ogg',
        'AbigailWilliamsOuterSkill1.ogg',
        'AbigailWilliamsOuterSkill2.ogg',
      ],
    },

    skills: [
      {
        id: 0,
        genericname: 'Stun',
        name: 'Gate lock',
        desc: 'Stuns the enemy for 2 turn. Cooldown of 5 turns',
        mpCost: 45,
        cooldown: 5,
        turnCooldown: 0,
        message: '<actor> locks <receiver> to the Gate of Outerworld. ',
        effects: [
          {
            target: 'enemy',
            type: 'status',
            status: 'stun', //put this on enum
            message: 'Stun <receiver> for <turn> turns',
            turns: 2,
          },
        ],
      },
      {
        id: 1,
        genericname: 'Def -',
        name: 'Abyss Gaze',
        desc: 'Decrease enemy defense for 4 turns. Cooldown of 6 turns',
        cooldown: 6,
        turnCooldown: 0,
        message: '<actor> sends shivering gaze to <receiver>. ',
        mpCost: 30,
        effects: [
          {
            target: 'enemy',
            type: 'status',
            status: 'defdown', //put this on enum
            modifier: 0.5,
            message: 'Decrease <receiver> defense for <turn> turns',
            turns: 4,
          },
        ],
      },
      {
        id: 2,
        genericname: 'Morph',
        name: 'Call thy Father - Yog-Sothoth',
        desc:
          'Transforms to Outer form. Increases Attack for 4 turns and Decrease Defense for 4 turns. Set HP to full. Cooldown of 9 turns',
        cooldown: 9,
        turnCooldown: 0,
        message: '<actor> transforms to Outer form. ',
        mpCost: 60,
        effects: [
          {
            target: 'self',
            type: 'status',
            status: 'atkup', //put this on enum
            modifier: 0.5,
            message: 'Increase <actor> attack for <turn> turns',
            turns: 4,
          },
          {
            target: 'self',
            type: 'status',
            status: 'defdown', //put this on enum
            modifier: 0.5,
            message: 'Decrease <actor> defense for <turn> turns',
            turns: 4,
          },
          {
            target: 'self',
            type: 'status',
            status: 'hpregen', //put this on enum
            modifier: 1000,
            message: '<actor> gains full health',
            turns: 1,
          },
        ],
      },
    ],

    baseAttack: 16,
    hpRegenRate: 3,
    maxHp: 150,
    mpRegenRate: 5,
    maxMp: 160,
  },
];
