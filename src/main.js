import Vue from 'vue';
import Vuelidate from 'vuelidate';
import VueResource from 'vue-resource';
import VTooltip from 'v-tooltip';
import VueRouter from 'vue-router';
import App from './App.vue';

import './shared/components';
import './shared/filters';
import { routes } from './router';
import { store } from './store';

const router = new VueRouter({
  routes,
  mode: 'hash',
});

Vue.use(VueRouter);
Vue.use(Vuelidate);
Vue.use(VTooltip);
Vue.use(VueResource);

export const eventBus = new Vue();
export const dataBus = new Vue({
  methods: {
    setAccountDetails(accountDetails) {
      this.accountDetails.username = accountDetails.username;
      this.accountDetails.password = accountDetails.password;
      this.accountDetails.fullName = accountDetails.fullName;
      this.accountDetails.email = accountDetails.email;
    },
    setCharacterDetails(classType, characterName) {
      this.accountDetails.classType = classType;
      this.accountDetails.characterName = characterName;
    },
    getAccountDetails() {
      return this.accountDetails;
    },
    setBattleResult(result) {
      this.battleResult = result;
    },
    getBattleResult() {
      return this.battleResult;
    },
    setPlayerData(name, character) {
      this.player = {
        name: name,
        className: character.className,
        detail: character.detail,
        isTurn: true,
        sprite: character.sprite,
        sounds: character.sounds,

        skills: character.skills,

        baseAttack: character.baseAttack,
        currentAttack: character.baseAttack,

        hpRegenRate: character.hpRegenRate,
        maxHp: character.maxHp,
        currentHp: character.maxHp,
        mpRegenRate: character.mpRegenRate,
        maxMp: character.maxMp,
        currentMp: character.maxMp,

        ailments: [],
      };
    },
    getPlayerData() {
      return this.player;
    },
  },
  data() {
    return {
      battleResult: {
        turnCount: 0,
      },
      player: {},
      accountId: '',
      accountDetails: {
        username: '',
        password: '',
        email: '',
        fullName: '',
        characterName: '',
        classType: 0,
      },
    };
  },
});
export const logger = new Vue({
  data() {
    return {
      logs: [],
    };
  },
  methods: {
    addLog(type, message) {
      const currentDateTime = new Date();
      const timestamp =
        currentDateTime.getFullYear() +
        '-' +
        (currentDateTime.getMonth() + 1) +
        '-' +
        currentDateTime.getDate() +
        ' ' +
        currentDateTime.getHours() +
        ':' +
        currentDateTime.getMinutes() +
        ':' +
        currentDateTime.getSeconds();
      this.logs.push(`[${timestamp}] ${message}`);
    },
    addQueuedLog(type, messages) {
      messages.forEach((message) => this.addLog(type, message));
    },
    getLogs() {
      return this.logs;
    },
    clearLogs() {
      this.logs = [];
    },
  },
});

new Vue({
  el: '#app',
  router,
  store,
  render: (h) => h(App),
});
