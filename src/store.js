import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    account: {},
    player: {},
    inventory: [],
    skills: [],
    dungeon: {},
    isCurrentEnemyBoss: false,
    enemy: {},
    isBattleWin: false,
  },
  mutations: {
    updatePlayer: (state, player) => {
      state.player = player;
    },
    updateSkills: (state, skills) => {
      state.skills = skills;
    },
    updateDungeon: (state, dungeon) => {
      state.dungeon = dungeon;
    },
    updateEnemy: (state, enemy) => {
      state.enemy = enemy;
    },
    reset: (state) => {
      state.account = {};
      state.player = {};
      state.inventory = [];
      state.skills = [];
      state.dungeon = {};
      state.isCurrentEnemyBoss = false;
      state.enemy = {};
      state.isBattleWin = false;
    },
    reenterDungeon: (state) => {
      state.isCurrentEnemyBoss = false;
      state.enemy = {};
      state.isBattleWin = false;
    },
  },
});
